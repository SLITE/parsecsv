<!DOCTYPE HTML>
<html>
 <head>
  <title>CSV</title>
  <meta charset="utf-8">
 </head>
 <body >
<?
require('Executor.php');
	try
	{
		getData();
	}
	catch(PDOException $msg)
	{
		echo "Operation failed: connection failed";
	}
	catch(Exception $msg)
	{
		echo "Undefined error.";
	}


	function getData()
	{
		$executor = new Executor("test");
		$dataFromDB = null;
		try
		{
			$dataFromDB = $executor->getRandRow();
		}
		catch(Exception $msg){
			echo $msg;
		}
		if($dataFromDB == null ){
			$executor->exportToDB("test.csv");
			$dataFromDB = $executor->getRandRow();
		}
		$executor->changeRowByID($dataFromDB["id"],$dataFromDB["status"]);
		$status = $dataFromDB["status"]==1?0:1;
		echo $dataFromDB["name"].";".$status;
		
	}

	
?>
</body>
</html>