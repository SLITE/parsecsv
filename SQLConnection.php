<?
/**
* Class descride  connecion to database
*/
class SQLConnect 
{
	var $host = "localhost";
	var $user = "root";
	var $password = "root";
	var $dbName = "example";
	var $connection = null;

	private static $instance;

	private function __construct()
	{
		$this->connection = new PDO("mysql:host={$this->host};dbname={$this->dbName};charset=utf8", $this->user, $this->password); 
		$this->connection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );   
	}

	public function getConnection(){
		return $this->connection;
	}

	private function __clone()
	{
		
	}

	public function getInstance()
	{	
		if(is_null(self::$instance)){
			$instance = new self();
		}
		return $instance;
	}
}
?>