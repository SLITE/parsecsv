<?
require_once('SQLConnection.php');
/**
* 
*/
class Executor
{
	public function __construct()
	{
		$this->connection = SQLConnect::getInstance()->getConnection();
	}

	public function Executor($table){
		$this->tableName = $table;
		$this->connection = SQLConnect::getInstance()->getConnection();
	}

	private $connection = null;
	public $tableName = "test";

	private function tableExists($pdo, $table) {
    try {
        $result = $pdo->query("SELECT 1 FROM {$table} LIMIT 1");
    } catch (Exception $e) {
        return FALSE;
    }
    	return $result !== FALSE;
	}

	private function convert($str){
		return iconv( "Windows-1251", "UTF-8", $str);
	}

	public function getRandRow(){
		if(!$this->tableExists($this->connection,$this->tableName)) 
		{
			return null;
		}
		else {
			$query = "SELECT * FROM {$this->tableName} order by RAND() LIMIT 1";
			$result = $this->connection->query($query); 
			$result->setFetchMode(PDO::FETCH_ASSOC); 
			if($row = $result->fetch()){
				return $row;
			}
			else {
				return array();
			}
		}
	}

	public function readFile($filePath){
		$result = [];
		$handle = fopen("test.csv", "r");
		while (($line = fgets($handle))!==false) {
		    $result[] = explode(";",$line); 

		}
		fclose($handle);
		return $result;
	}

	public function exportToDB($file){
		if($this->connection){
			$create = "CREATE TABLE ".$this->tableName." (".
			"id INT(5) UNSIGNED AUTO_INCREMENT PRIMARY KEY,".
			"name  VARCHAR(30) CHARACTER SET utf8 NOT NULL,".
			"status TINYINT NOT NULL) DEFAULT CHARSET=utf8";
			$this->connection->exec($create);
			$dataFromFile = $this->readFile($file);
			for($i=1;$i<count($dataFromFile);$i++){
				$data = $dataFromFile[$i];
				$insert = $this->connection->prepare("INSERT INTO {$this->tableName} (name, status) values (?, ?)"); 
				$insert->bindParam(1, $this->convert($data[0]));
				$insert->bindParam(2, $this->convert($data[1]));    
				$insert->execute();
			}
		}
	}

	public function changeRowByID($id,$status){
		$changedStatus = $status==1?0:1;
		$update = $this->connection->prepare("UPDATE {$this->tableName} set status = ? WHERE id = ?"); 
		$update->bindParam(1, $changedStatus);
		$update->bindParam(2, $id);
		$update->execute();
	}
		
}
?>